# parrot-challenge-service

# Prueba técnica.

### Live Coding
Se tiene que crear una API que será consumida por un conjunto de clientes (móviles y
web).

Tiene que contener los siguientes servicios web, almacenando los datos necesarios
para su correcto funcionamiento.

- Crear Ordenes para un Comensal. 
  + Las órdenes serán creadas por un Usuario operador. 
    - Nombre del comensal quien pidió la orden.
    - Precio Total de la orden.
    - Lista de Productos que conforman la Orden
    

- Las Ordenes deben contener la lista de Productos que la conforman.
- Los Productos son creados por el Usuario al mismo tiempo que se crea la Orden. 
- Productos con el mismo nombre será considerados como iguales
    + Nombre del producto.
    + Precio unitario.
    + Cantidad.
    
- Reporte de productos vendidos.
    + Filtrado por fecha (inicio y fin) y que Usuario los vendió.
    + Ordenado por Producto de mayor a menor vendido.
    + Las Columnas necesarias para el reporte son Nombre del producto, Cantidad Total, Precio Total.

