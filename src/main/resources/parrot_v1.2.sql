DROP DATABASE IF EXISTS parrotV2;
CREATE DATABASE parrotV2;
USE parrotV2;

CREATE TABLE roles (
	idRol INT AUTO_INCREMENT,
	rolName VARCHAR(50) NOT NULL,
	PRIMARY KEY(idRol)
);

INSERT INTO roles(rolName) VALUES ('Operador');
INSERT INTO roles(rolName) VALUES ('Admin');

CREATE TABLE users (
	idUser INT AUTO_INCREMENT,
	idRol INT NOT NULL DEFAULT 1,
	createdTime DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	updatedTime DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	email VARCHAR(100) NOT NULL UNIQUE,
	pw VARCHAR(100) NOT NULL,
	name VARCHAR(100) NOT NULL,
	lastName VARCHAR(100) NOT NULL,
	PRIMARY KEY(idUser),
	FOREIGN KEY(idRol) REFERENCES roles(idRol)
);

INSERT INTO users(idRol, email, pw, name, lastName) VALUES (1, 'devluis1@parrot.com', '12345', 'Luis', 'Zepeda');
INSERT INTO users(idRol, email, pw, name, lastName) VALUES (1, 'devluis2@parrot.com', '12346', 'Luis', 'ZG');

CREATE TABLE comensal (
	idComensal INT AUTO_INCREMENT,
	createdTime DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	name VARCHAR(100) NOT NULL,
	PRIMARY KEY(idComensal)
);

CREATE TABLE orders (
	idOrder INT AUTO_INCREMENT,
	idUser INT NOT NULL,
	idComensal INT NOT NULL,
    createdTime DATE NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY(idOrder),
	FOREIGN KEY(idUser) REFERENCES users(idUser),
	FOREIGN KEY(idComensal) REFERENCES comensal(idComensal)
);

CREATE TABLE products (
	idProducto INT AUTO_INCREMENT,
	createdTime DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	updatedTime DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	productName VARCHAR(100) NOT NULL,
	unitPrice FLOAT NOT NULL,
	quantity INT DEFAULT NULL,
	PRIMARY KEY(idProducto)
);

CREATE TABLE order_product (
	idOrder INT NOT NULL,
	idProducto INT NOT NULL,
	createdDate DATE NOT NULL DEFAULT CURRENT_TIMESTAMP,
	productName VARCHAR(100) NOT NULL,
	unitPrice FLOAT NOT NULL,
	quantity INT DEFAULT NULL,
	FOREIGN KEY(idOrder) REFERENCES orders(idOrder),
	FOREIGN KEY(idProducto) REFERENCES products(idProducto)
);