package io.parrotsoftware.parrotchallengeservice.controller;

import io.parrotsoftware.parrotchallengeservice.model.OrdenResponse;
import io.parrotsoftware.parrotchallengeservice.model.OrderRequest;
import io.parrotsoftware.parrotchallengeservice.model.report.Report;
import io.parrotsoftware.parrotchallengeservice.model.report.ReportResponse;
import io.parrotsoftware.parrotchallengeservice.service.IOrderService;
import io.parrotsoftware.parrotchallengeservice.service.IReportService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

import static io.parrotsoftware.parrotchallengeservice.builder.ParrotBuilder.getHeaders;

@Slf4j
@RestController
@RequestMapping("/api/parrot")
public class ReportController {

    @Autowired
    private IReportService reportService;

    @RequestMapping(
            value = "/report",
            produces = {"application/json"},
            method = RequestMethod.GET
    )
    public Mono<ResponseEntity<ReportResponse>> createUser (
            ServerWebExchange serverWebExchange,
            @RequestHeader(value = "Authorization", required = true) String authorization,
            @Valid @RequestParam(value = "start_date", required = true) String startDate,
            @Valid @RequestParam(value = "end_date", required = true) String endDate) {

        log.info("Request Headers :: {}", getHeaders(serverWebExchange));

        return reportService.getDataForReport(getHeaders(serverWebExchange), startDate, endDate)
                .map(response -> {

                    log.info("Final response {}", response);

                    return new ResponseEntity<>(response, HttpStatus.OK);
                });
    }
}
