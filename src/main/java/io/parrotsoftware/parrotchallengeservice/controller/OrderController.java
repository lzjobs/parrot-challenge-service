package io.parrotsoftware.parrotchallengeservice.controller;

import io.parrotsoftware.parrotchallengeservice.model.OrdenResponse;
import io.parrotsoftware.parrotchallengeservice.model.OrderRequest;
import io.parrotsoftware.parrotchallengeservice.service.IOrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

import static io.parrotsoftware.parrotchallengeservice.builder.ParrotBuilder.getHeaders;

@Slf4j
@RestController
@RequestMapping("/api/parrot")
public class OrderController {

    @Autowired
    private IOrderService orderService;

    @RequestMapping(
            value = "/order/add",
            produces = {"application/json"},
            consumes = {"application/json"},
            method = RequestMethod.POST
    )
    public Mono<ResponseEntity<OrdenResponse>> createUser (
            ServerWebExchange serverWebExchange,
            @RequestHeader(value = "Authorization", required = true) String authorization,
            @Valid @RequestBody OrderRequest orderRequest) {

        log.info("Request Headers :: {}", getHeaders(serverWebExchange));

        return orderService.createOrder(getHeaders(serverWebExchange), orderRequest)
                .map(response -> {

                    log.info("Final response {}", response);

                    return new ResponseEntity<>(response, HttpStatus.CREATED);
                });
    }
}
