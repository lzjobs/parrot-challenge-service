package io.parrotsoftware.parrotchallengeservice.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ParrotErrors {

    PARROT_ERR_000("PARROT-ERROR-000", "Se debe informar el header 'Authorization'"),
    PARROT_ERR_001("PARROT-ERROR-001", "Se debe informar los datos para orden."),
    PARROT_ERR_002("PARROT-ERROR-002", "Ocurrió un error al registrar la orden."),
    PARROT_ERR_003("PARROT-ERROR-003", "Ocurrió un error al registrar la orden de productos."),
    PARROT_ERR_004("PARROT-ERROR-004", "Error el formato de la fecha del parámetro %s es incorrecto."),
    PARROT_ERR_005("PARROT-ERROR-005", "No se encontró información con los datos proporcionados");

    private String code;
    private String message;
}
