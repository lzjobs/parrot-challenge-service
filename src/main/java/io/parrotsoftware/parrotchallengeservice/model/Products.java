package io.parrotsoftware.parrotchallengeservice.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Products {

    @JsonProperty("idProducto")
    private Integer idProducto;

    @JsonProperty("createdTime")
    private String createdTime;

    @JsonProperty("updatedTime")
    private String updatedTime;

    @JsonProperty("productName")
    private String productName;

    @JsonProperty("unitPrice")
    private float unitPrice;

    @JsonProperty("quantity")
    private Integer quantity;
}
