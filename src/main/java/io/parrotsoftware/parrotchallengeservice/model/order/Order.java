package io.parrotsoftware.parrotchallengeservice.model.order;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.parrotsoftware.parrotchallengeservice.model.Comensal;
import io.parrotsoftware.parrotchallengeservice.model.Products;
import io.parrotsoftware.parrotchallengeservice.model.Users;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Order {

    @JsonProperty("user")
    private Users user;

    @JsonProperty("comensal")
    private Comensal comensal;

    @JsonProperty("productos")
    private List<Products> productos;
}
