package io.parrotsoftware.parrotchallengeservice.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Orders {

    @JsonProperty("idOrder")
    private Integer idOrder;

    @JsonProperty("idUser")
    private Integer idUser;

    @JsonProperty("idComensal")
    private Integer idComensal;

    @JsonProperty("createdTime")
    private String createdTime;
}
