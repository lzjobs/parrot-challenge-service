package io.parrotsoftware.parrotchallengeservice.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Users {

    @JsonProperty("idUser")
    private Integer idUser;

    @JsonProperty("idRol")
    private Integer idRol;

    @JsonProperty("createdTime")
    private String createdTime;

    @JsonProperty("updatedTime")
    private String updatedTime;

    @JsonProperty("email")
    private String email;

    @JsonProperty("pw")
    private String pw;

    @JsonProperty("name")
    private String name;

    @JsonProperty("lastName")
    private String lastName;
}
