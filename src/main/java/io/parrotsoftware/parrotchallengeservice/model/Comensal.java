package io.parrotsoftware.parrotchallengeservice.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Comensal {

    @JsonProperty("idComensal")
    private Integer idComensal;

    @JsonProperty("createdTime")
    private String createdTime;

    @JsonProperty("name")
    private String name;
}
