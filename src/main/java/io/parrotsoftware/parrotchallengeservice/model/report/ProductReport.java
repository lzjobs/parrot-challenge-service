package io.parrotsoftware.parrotchallengeservice.model.report;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProductReport {

    @JsonProperty("productName")
    private String productName;

    @JsonProperty("unitPrice")
    private Float unitPrice;

    @JsonProperty("quantity")
    private Integer quantity;

    @JsonProperty("totalAmount")
    private Float totalAmount;
}
