package io.parrotsoftware.parrotchallengeservice.model.report;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Report {

    @JsonProperty("date")
    private String date;

    @JsonProperty("userName")
    private String userName;

    @JsonProperty("totalAmount")
    private Float totalAmount;

    @JsonProperty("productReportList")
    private List<ProductReport> productReportList;
}
