package io.parrotsoftware.parrotchallengeservice.transform;

import io.parrotsoftware.parrotchallengeservice.model.Comensal;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

@Slf4j
@Component
public class ComensalTransform {

    public Comensal getComensal (final List<Map<String, Object>> rows) {

        final String idComensal = Optional.ofNullable(rows.get(0).get("idComensal")).map(Objects::toString).orElse("");
        final String createdTime = Optional.ofNullable(rows.get(0).get("createdTime")).map(Objects::toString).orElse("");
        final String name = Optional.ofNullable(rows.get(0).get("name")).map(Objects::toString).orElse("");

        Comensal comensal = new Comensal();

        comensal.setIdComensal(Integer.valueOf(idComensal));
        comensal.setCreatedTime(createdTime);
        comensal.setName(name);

        return comensal;
    }
}
