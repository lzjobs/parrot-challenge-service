package io.parrotsoftware.parrotchallengeservice.transform;

import io.parrotsoftware.parrotchallengeservice.model.Products;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

@Slf4j
@Component
public class ProductsTransform {

    public Products getProduct (final List<Map<String, Object>> rows) {

        final String productName = Optional.ofNullable(rows.get(0).get("productName")).map(Objects::toString).orElse("");
        final String unitPrice = Optional.ofNullable(rows.get(0).get("unitPrice")).map(Objects::toString).orElse("");
        final String quantity = Optional.ofNullable(rows.get(0).get("quantity")).map(Objects::toString).orElse("");

        Products products = new Products();

        products.setProductName(productName);
        products.setUnitPrice(Float.parseFloat(unitPrice));
        products.setQuantity(Integer.parseInt(quantity));

        return products;
    }
}
