package io.parrotsoftware.parrotchallengeservice.transform;

import io.parrotsoftware.parrotchallengeservice.model.OrdenResponse;
import io.parrotsoftware.parrotchallengeservice.model.OrderRequest;
import io.parrotsoftware.parrotchallengeservice.model.Products;
import io.parrotsoftware.parrotchallengeservice.model.order.Order;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Component
public class OrderTransform {

    public OrdenResponse responseOrder (OrderRequest orderRequest, final Integer idOrder) {

        OrdenResponse ordenResponse = new OrdenResponse();
        ordenResponse.setOrderRequest(orderRequest);
        ordenResponse.setIdOrder(idOrder);
        ordenResponse.setTotal(getTotalAmount(orderRequest));

        return ordenResponse;
    }

    private float getTotalAmount (OrderRequest orderRequest) {

        List<Products> productos = Optional.ofNullable(orderRequest).map(OrderRequest::getOrden).map(Order::getProductos).orElse(null);
        List<Float> totales = new ArrayList<>();

        if (!CollectionUtils.isEmpty(productos)) {
            productos.forEach(product -> totales.add(product.getQuantity() * product.getUnitPrice()));
        }

        return (float) totales.stream().mapToDouble(o -> o).sum();
    }
}
