package io.parrotsoftware.parrotchallengeservice.transform;

import io.parrotsoftware.parrotchallengeservice.model.Users;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

@Slf4j
@Component
public class UserTransform {

    public Users getUserInformation (List<Map<String, Object>> rows) {

        final String name = Optional.ofNullable(rows.get(0).get("name")).map(Objects::toString).orElse("");
        final String lastName = Optional.ofNullable(rows.get(0).get("lastName")).map(Objects::toString).orElse("");

        Users users = new Users();

        users.setName(name);
        users.setLastName(lastName);

        return users;
    }
}
