package io.parrotsoftware.parrotchallengeservice.transform;

import io.parrotsoftware.parrotchallengeservice.model.Orders;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

@Slf4j
@Component
public class OrdersTransform {

    public Orders getOrder (final List<Map<String, Object>> rows) {

        final String idOrder = Optional.ofNullable(rows.get(0).get("idOrder")).map(Objects::toString).orElse("");
        final String idUser = Optional.ofNullable(rows.get(0).get("idUser")).map(Objects::toString).orElse("");
        final String idComensal = Optional.ofNullable(rows.get(0).get("idComensal")).map(Objects::toString).orElse("");
        final String createdTime = Optional.ofNullable(rows.get(0).get("createdTime")).map(Objects::toString).orElse("");

        Orders orders = new Orders();

        orders.setIdOrder(Integer.valueOf(idOrder));
        orders.setIdUser(Integer.valueOf(idUser));
        orders.setIdComensal(Integer.valueOf(idComensal));
        orders.setCreatedTime(createdTime);

        return orders;
    }
}
