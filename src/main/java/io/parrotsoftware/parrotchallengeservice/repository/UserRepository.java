package io.parrotsoftware.parrotchallengeservice.repository;

import io.parrotsoftware.parrotchallengeservice.model.Users;
import io.parrotsoftware.parrotchallengeservice.transform.UserTransform;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

@Slf4j
@Repository
public class UserRepository implements IUserRepository {

    @Autowired
    @Qualifier("jdbcConnection")
    private JdbcTemplate connection;

    @Autowired
    private UserTransform userTransform;

    @Override
    public Users getUserById(Integer idUser) {

        try {

            final StringBuilder query = new StringBuilder("SELECT * FROM ").append("users WHERE idUser = ?");

            List<Map<String, Object>> rows = connection.queryForList(query.toString(), idUser);

            if (!CollectionUtils.isEmpty(rows)) {

                final String data = Optional.ofNullable(rows.get(0)).map(Objects::toString).orElse("");

                log.info("DATA {}", data);

                return userTransform.getUserInformation(rows);
            }

        } catch (DataAccessException e) {
            log.error("Error al buscar el usuario :: {}", e.toString());
        }

        return new Users();
    }
}
