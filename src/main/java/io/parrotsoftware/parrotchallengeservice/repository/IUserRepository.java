package io.parrotsoftware.parrotchallengeservice.repository;


import io.parrotsoftware.parrotchallengeservice.model.Users;

public interface IUserRepository {
    Users getUserById (final Integer idUser);
}
