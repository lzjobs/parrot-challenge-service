package io.parrotsoftware.parrotchallengeservice.repository;

/**
 * {@link ICommonRepository} define el método que permite obtener el id del ultimo registro realizado en una tabla.
 *
 * @author luisz
 */
public interface ICommonRepository {

    /**
     * Permite recuperar el id del ultimo registro realizado.
     * @param tableName Nombre de la tabla de la cual se requiere obtener el id del ultimo registro.
     * @param idName Nombre del campo que identifica al id.
     * @return @return {@link Integer} que representa el id del ultimo registro realizado.
     */
    Integer getLastIdentifier(final String tableName, final String idName);
}
