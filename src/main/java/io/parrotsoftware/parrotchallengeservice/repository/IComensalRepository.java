package io.parrotsoftware.parrotchallengeservice.repository;

import io.parrotsoftware.parrotchallengeservice.model.Comensal;

/**
 * {@link IComensalRepository} define los métodos que permiten operar sobre la tabla comensal.
 *
 * @author luisz
 */
public interface IComensalRepository {

    /**
     * Permite realizar el registro de un comensal.
     * @param comensal Datos del comensal a registrar.
     * @return {@link Boolean} que indica si se registro correctamente al comensal.
     * True: Registro correcto
     * False: Error durante el registro
     */
    Boolean createComensal(final Comensal comensal);

    /**
     * Permite recuperar un comensal por id.
     * @param idComensal Id del comensal a buscar en los registros de la BD.
     * @return {@link Comensal}
     */
    Comensal getComensalById(final Integer idComensal);
}
