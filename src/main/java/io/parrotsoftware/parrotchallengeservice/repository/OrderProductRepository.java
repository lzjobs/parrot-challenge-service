package io.parrotsoftware.parrotchallengeservice.repository;

import io.parrotsoftware.parrotchallengeservice.model.OrderProduct;
import io.parrotsoftware.parrotchallengeservice.model.Products;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * {@link OrderProductRepository} implementa los métodos que permiten operar sobre la tabla order_product.
 *
 * @author luisz
 */
@Slf4j
@Repository
public class OrderProductRepository implements IOrderProductRepository {

    @Autowired
    @Qualifier("jdbcConnection")
    private JdbcTemplate connection;

    /**
     * Permite crear una orden de producto.
     *
     * @param idProducto Id del producto inicialmente registrado en tabla productos.
     * @param products   Producto a registrar en la orden.
     * @return {@link Boolean} que indica si se registro correctamente la orden de producto.
     * True: Registro correcto
     * False: Error durante el registro
     */
    @Override
    public Boolean createOrderProduct(final Integer idOrder, Integer idProducto, Products products) {

        try {

            final StringBuilder query = new StringBuilder("INSERT INTO ").append("order_product (idOrder, idProducto, productName, unitPrice, quantity) VALUES (?, ?, ?, ?, ?)");

            int executeQuery = connection.update(query.toString(), idOrder, idProducto, products.getProductName(), products.getUnitPrice(), products.getQuantity());

            return (executeQuery != 0);

        } catch (DataAccessException e) {
            log.error("Error al registrar la orden de producto :: {}", e.toString());
        }
        return false;
    }

    @Override
    public List<OrderProduct> getOrderProductsByIdOrder(Integer idOrder) {

        final StringBuilder query = new StringBuilder("SELECT * FROM ").append("order_product WHERE idOrder = ").append(idOrder);

        return connection.query(query.toString(), (rs, rowNum) -> {

            OrderProduct orderProduct = new OrderProduct();
            orderProduct.setIdOrder(rs.getInt("idOrder"));
            orderProduct.setIdProducto(rs.getInt("idProducto"));
            orderProduct.setProductName(rs.getString("productName"));
            orderProduct.setUnitPrice(rs.getFloat("unitPrice"));
            orderProduct.setQuantity(rs.getInt("quantity"));

            return orderProduct;
        });
    }
}
