package io.parrotsoftware.parrotchallengeservice.repository;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * {@link CommonRepository} implementa el método que permite obtener el id del ultimo registro realizado en una tabla.
 *
 * @author luisz
 */
@Slf4j
@Repository
public class CommonRepository implements ICommonRepository {

    /**
     * Inyección de dependencia {@link JdbcTemplate} para la conexión de BD.
     */
    @Autowired
    @Qualifier("jdbcConnection")
    private JdbcTemplate connection;

    /**
     * Permite recuperar el id del ultimo registro realizado.
     *
     * @param tableName Nombre de la tabla de la cual se requiere obtener el id del ultimo registro.
     * @param idName    Nombre del campo que identifica al id.
     *
     * @return @return {@link Integer} que representa el id del ultimo registro realizado.
     */
    @Override
    public Integer getLastIdentifier(final String tableName, final String idName) {

        try {

            final StringBuilder query = new StringBuilder("SELECT * FROM ").append(tableName).append(" ORDER BY ").append(idName).append(" DESC LIMIT 1");

            List<Map<String, Object>> rows = connection.queryForList(query.toString());

            if (!CollectionUtils.isEmpty(rows)) {

                final String id = Optional.ofNullable(rows.get(0).get(idName)).map(Objects::toString).orElse("");

                return Integer.parseInt(id);
            }

        } catch (DataAccessException e) {
            log.error("Error al obtener el ultimo Id registrado en tabla {} :: {}", tableName, e.toString());
        }

        return 0;
    }
}
