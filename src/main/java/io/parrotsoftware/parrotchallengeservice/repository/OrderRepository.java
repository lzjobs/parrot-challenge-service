package io.parrotsoftware.parrotchallengeservice.repository;

import io.parrotsoftware.parrotchallengeservice.model.Orders;
import io.parrotsoftware.parrotchallengeservice.transform.OrdersTransform;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Map;

/**
 * {@link OrderRepository} implementa los métodos que permiten operar sobre la tabla orders.
 *
 * @author luisz
 */
@Slf4j
@Repository
public class OrderRepository implements IOrderRepository {

    @Autowired
    @Qualifier("jdbcConnection")
    private JdbcTemplate connection;

    @Autowired
    private OrdersTransform ordersTransform;

    /**
     * Permite dar de alta una orden de compra.
     *
     * @param idUser     Id del usuario que da seguimiento y/o ejecuta la orden.
     * @param idComensal Id del comensal que realiza el pedido.
     * @return {@link Boolean} que indica si se registro correctamente la orden.
     * True: Registro correcto
     * False: Error durante el registro
     */
    @Override
    public Boolean createOrder(final Integer idUser, final Integer idComensal) {

        try {

            final StringBuilder query = new StringBuilder("INSERT INTO ").append("orders (idUser, idComensal) VALUES (?, ?)");

            int executeQuery = connection.update(query.toString(), idUser, idComensal);

            return (executeQuery != 0);

        } catch (DataAccessException e) {
            log.error("Error al registrar la orden :: {}", e.toString());
        }
        return false;
    }

    /**
     * Permite recuperar una orden por medio del identificador.
     *
     * @param idOrder Id de la orden a buscar en los registros de la BD.
     * @return {@link Orders} del registro localizado.
     */
    @Override
    public Orders getOrderById(final Integer idOrder) {

        try {

            final StringBuilder query = new StringBuilder("SELECT * FROM ").append("orders WHERE idOrder = ?");

            List<Map<String, Object>> rows = connection.queryForList(query.toString(), idOrder);

            if (!CollectionUtils.isEmpty(rows)) {
                return ordersTransform.getOrder(rows);
            }

        } catch (DataAccessException e) {
            log.error("Error al obtener los datos del comensal indicado :: {}", e.toString());
        }

        return new Orders();
    }

    /**
     * Permite filtrar las ordenes por rangos de fechas.
     *
     * @param initDate Rango de fecha inicial para la búsqueda de registros.
     * @param endDate  Rango de fecha final para la búsqueda de registros.
     * @return {@link List<Orders>} con los registros que cumplen la condición.
     */
    @Override
    public List<Orders> getOrdersBySaleDate(final String initDate, final String endDate) {

        final StringBuilder query = new StringBuilder("SELECT * FROM ").append("orders WHERE createdTime BETWEEN '").append(initDate).append("' AND '").append(endDate).append("'");

        return connection.query(query.toString(), (rs, rowNum) -> {

            Orders orders = new Orders();
            orders.setIdOrder(rs.getInt("idOrder"));
            orders.setIdUser(rs.getInt("idUser"));
            orders.setIdComensal(rs.getInt("idComensal"));
            orders.setCreatedTime(rs.getString("createdTime"));

            return orders;
        });

    }
}
