package io.parrotsoftware.parrotchallengeservice.repository;

import io.parrotsoftware.parrotchallengeservice.model.Orders;

import java.util.List;

/**
 * {@link IOrderRepository} define los métodos que permiten operar sobre la tabla orders.
 *
 * @author luisz
 */
public interface IOrderRepository {

    /**
     * Permite dar de alta una orden de compra.
     * @param idUser Id del usuario que da seguimiento y/o ejecuta la orden.
     * @param idComensal Id del comensal que realiza el pedido.
     * @return {@link Boolean} que indica si se registro correctamente la orden.
     * True: Registro correcto
     * False: Error durante el registro
     */
    Boolean createOrder (final Integer idUser, final Integer idComensal);

    /**
     * Permite recuperar una orden por medio del identificador.
     * @param idOrder Id de la orden a buscar en los registros de la BD.
     * @return {@link Orders} del registro localizado.
     */
    Orders getOrderById(final Integer idOrder);

    /**
     * Permite filtrar las ordenes por rangos de fechas.
     * @param initDate Rango de fecha inicial para la búsqueda de registros.
     * @param endDate Rango de fecha final para la búsqueda de registros.
     * @return {@link List<Orders>} con los registros que cumplen la condición.
     */
    List<Orders> getOrdersBySaleDate(final String initDate, final String endDate);
}
