package io.parrotsoftware.parrotchallengeservice.repository;

import io.parrotsoftware.parrotchallengeservice.model.Comensal;
import io.parrotsoftware.parrotchallengeservice.transform.ComensalTransform;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Map;

/**
 * {@link ComensalRepository} implementa los métodos que permiten operar sobre la tabla comensal.
 *
 * @author luisz
 */
@Slf4j
@Repository
public class ComensalRepository implements IComensalRepository {

    /**
     * Inyección de dependencia {@link JdbcTemplate} para la conexión de BD.
     */
    @Autowired
    @Qualifier("jdbcConnection")
    private JdbcTemplate connection;

    /**
     * Inyección de dependencia {@link ComensalTransform} para transformar la información de la BD.
     */
    @Autowired
    private ComensalTransform comensalTransform;

    /**
     * Permite realizar el registro de un comensal.
     *
     * @param comensal Datos del comensal a registrar.
     * @return {@link Boolean} que indica si se registro correctamente al comensal.
     * True: Registro correcto
     * False: Error durante el registro
     */
    @Override
    public Boolean createComensal(final Comensal comensal) {

        try {

            final StringBuilder query = new StringBuilder("INSERT INTO ").append("comensal (name) VALUES (?)");

            int executeQuery = connection.update(query.toString(), comensal.getName());

            return (executeQuery != 0);

        } catch (DataAccessException e) {
            log.error("Error al registrar los datos del comensal :: {}", e.toString());
        }

        return false;
    }

    /**
     * Permite recuperar un comensal por id.
     *
     * @param idComensal Id del comensal a buscar en los registros de la BD.
     * @return {@link Comensal}
     */
    @Override
    public Comensal getComensalById(final Integer idComensal) {

        try {

            final StringBuilder query = new StringBuilder("SELECT * FROM ").append("comensal WHERE idComensal = ?");

            List<Map<String, Object>> rows = connection.queryForList(query.toString(), idComensal);

            if (!CollectionUtils.isEmpty(rows)) {
                return comensalTransform.getComensal(rows);
            }

        } catch (DataAccessException e) {
            log.error("Error al obtener los datos del comensal indicado :: {}", e.toString());
        }

        return new Comensal();
    }
}
