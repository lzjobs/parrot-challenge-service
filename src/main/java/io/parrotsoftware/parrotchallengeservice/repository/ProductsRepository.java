package io.parrotsoftware.parrotchallengeservice.repository;

import io.parrotsoftware.parrotchallengeservice.model.Products;
import io.parrotsoftware.parrotchallengeservice.transform.ProductsTransform;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Map;

/**
 * {@link ProductsRepository} implementa los métodos que permiten operar sobre la tabla products.
 *
 * @author luisz
 */
@Slf4j
@Repository
public class ProductsRepository implements IProductsRepository {

    @Autowired
    @Qualifier("jdbcConnection")
    private JdbcTemplate connection;

    @Autowired
    private ProductsTransform productsTransform;

    /**
     * Permite registrar un producto.
     *
     * @param product Producto a registrar.
     * @return {@link Boolean} que indica si se registro correctamente el producto.
     * True: Registro correcto
     * False: Error durante el registro
     */
    @Override
    public Boolean createProduct(Products product) {

        try {

            final StringBuilder queryProduct = new StringBuilder("INSERT INTO ").append("products (productName, unitPrice, quantity) VALUES (?, ?, ?)");

            int executeQuery = connection.update(queryProduct.toString(), product.getProductName(), product.getUnitPrice(), product.getQuantity());

            return (executeQuery != 0);

        } catch (DataAccessException e) {
            log.error("Error al registrar el producto :: {}", e.toString());
        }
        return false;
    }

    /**
     * Obtiene un producto por id.
     *
     * @param idProducto Id del producto a buscar.
     * @return {@link Products} con la información del producto buscado.
     */
    @Override
    public Products getProductById(final Integer idProducto) {

        try {

            final StringBuilder query = new StringBuilder("SELECT * FROM ").append("products WHERE idProducto = ?");

            List<Map<String, Object>> rows = connection.queryForList(query.toString(), idProducto);

            if (!CollectionUtils.isEmpty(rows)) {
                return productsTransform.getProduct(rows);
            }

        } catch (DataAccessException e) {
            log.error("Error al obtener los datos del comensal indicado :: {}", e.toString());
        }

        return new Products();
    }
}
