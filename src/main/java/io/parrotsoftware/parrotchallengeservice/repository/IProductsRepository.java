package io.parrotsoftware.parrotchallengeservice.repository;

import io.parrotsoftware.parrotchallengeservice.model.Products;

/**
 * {@link IProductsRepository} define los métodos que permiten operar sobre la tabla products.
 *
 * @author luisz
 */
public interface IProductsRepository {

    /**
     * Permite registrar un producto.
     * @param product Producto a registrar.
     * @return {@link Boolean} que indica si se registro correctamente el producto.
     * True: Registro correcto
     * False: Error durante el registro
     */
    Boolean createProduct (final Products product);

    /**
     * Obtiene un producto por id.
     * @param idProducto Id del producto a buscar.
     * @return {@link Products} con la información del producto buscado.
     */
    Products getProductById(final Integer idProducto);
}
