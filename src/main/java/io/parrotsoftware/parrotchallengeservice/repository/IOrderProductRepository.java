package io.parrotsoftware.parrotchallengeservice.repository;

import io.parrotsoftware.parrotchallengeservice.model.OrderProduct;
import io.parrotsoftware.parrotchallengeservice.model.Products;

import java.util.List;

/**
 * {@link IOrderProductRepository} define los métodos que permiten operar sobre la tabla order_product.
 *
 * @author luisz
 */
public interface IOrderProductRepository {

    /**
     * Permite crear una orden de producto.
     * @param idProducto Id del producto inicialmente registrado en tabla productos.
     * @param products Producto a registrar en la orden.
     * @return {@link Boolean} que indica si se registro correctamente la orden de producto.
     * True: Registro correcto
     * False: Error durante el registro
     */
    Boolean createOrderProduct (final Integer idOrder, final Integer idProducto, final Products products);

    List<OrderProduct> getOrderProductsByIdOrder(final Integer idOrder);
}
