package io.parrotsoftware.parrotchallengeservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {
        "io.parrotsoftware.parrotchallengeservice",
        "com.pinncode.libs.httpwebclient",
        "com.pinncode.libs.exceptionhandler"
})
public class ParrotApplication {

    public static void main(String[] args) {
        SpringApplication.run(ParrotApplication.class, args);
    }

}
