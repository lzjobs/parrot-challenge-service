package io.parrotsoftware.parrotchallengeservice.builder;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.web.server.ServerWebExchange;

import java.util.Map;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ParrotBuilder {

    public static Map<String, String> getHeaders (final ServerWebExchange serverWebExchange) {

        final HttpHeaders httpHeaders = serverWebExchange.getRequest().getHeaders();

        return httpHeaders.toSingleValueMap();
    }
}
