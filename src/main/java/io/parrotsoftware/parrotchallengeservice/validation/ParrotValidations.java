package io.parrotsoftware.parrotchallengeservice.validation;

import com.pinncode.libs.exceptionhandler.exception.CodeException;
import io.parrotsoftware.parrotchallengeservice.model.OrderRequest;
import io.parrotsoftware.parrotchallengeservice.model.order.Order;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import reactor.core.publisher.Mono;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import static io.parrotsoftware.parrotchallengeservice.exception.ParrotErrors.*;
import static org.apache.logging.log4j.util.Strings.EMPTY;

@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ParrotValidations {

    public static Mono<Boolean> validateOrder (final Map<String, String> headers, final OrderRequest orderRequest) {

        return validateAuthorization(headers)
                .flatMap(aBoolean -> validateAuthorization(orderRequest));
    }

    public static Mono<Boolean> validateAuthorization (final Map<String, String> headers) {

        final String authorization = Optional.ofNullable(headers).map(h -> h.get("Authorization")).map(Objects::toString).orElse(EMPTY);

        if (StringUtils.isBlank(authorization)) {
            return Mono.error(new CodeException(PARROT_ERR_000.getCode(), PARROT_ERR_000.getMessage(), HttpStatus.UNAUTHORIZED));
        }

        return Mono.just(true);
    }

    private static Mono<Boolean> validateAuthorization (final OrderRequest orderRequest) {

        final Order orden = Optional.ofNullable(orderRequest).map(OrderRequest::getOrden).orElse(null);

        if (orden == null) {
            return Mono.error(new CodeException(PARROT_ERR_001.getCode(), PARROT_ERR_001.getMessage(), HttpStatus.UNAUTHORIZED));
        }

        return Mono.just(true);
    }

    public static Mono<Boolean> validateQueryDates (String startDate, String endDate) {

        if (Boolean.FALSE.equals(isValidDate(startDate))) {
            return Mono.error(new CodeException(PARROT_ERR_004.getCode(), String.format(PARROT_ERR_004.getMessage(), "start_date"), HttpStatus.BAD_REQUEST));
        }

        if (Boolean.FALSE.equals(isValidDate(endDate))) {
            return Mono.error(new CodeException(PARROT_ERR_004.getCode(), String.format(PARROT_ERR_004.getMessage(), "end_date"), HttpStatus.BAD_REQUEST));
        }

        return Mono.just(true);
    }

    private static Boolean isValidDate (final String date) {

        boolean valid = false;

        try {

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            simpleDateFormat.setLenient(false);
            simpleDateFormat.parse(date);

            valid = true;

        } catch (ParseException e) {

            log.info("Error en formato de fecha");
        }

        return valid;
    }
}
