package io.parrotsoftware.parrotchallengeservice.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

@Configuration
public class DBConfiguration {

    @Value("${datasource.mysql.driverClassName:}")
    private String driverClassName;

    @Value("${datasource.mysql.jdbcUrl:}")
    private String jdbcUrl;

    @Value("${datasource.mysql.username:}")
    private String username;

    @Value("${datasource.mysql.password:}")
    private String password;

    @Bean(name = "mysqldb")
    public DataSource mysqlDataSource() {

        DriverManagerDataSource dataSource = new DriverManagerDataSource();

        dataSource.setDriverClassName(driverClassName);
        dataSource.setUrl(jdbcUrl);
        dataSource.setUsername(username);
        dataSource.setPassword(password);

        return dataSource;
    }

    @Bean(name = "jdbcConnection")
    @Autowired
    public JdbcTemplate masterJdbcTemplate(@Qualifier("mysqldb") DataSource dsMaster) {
        return new JdbcTemplate(dsMaster);
    }
}
