package io.parrotsoftware.parrotchallengeservice.util;

import com.itextpdf.text.Font;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
public class Fonts {

    /**
     * Variables globales, definición de tipos y tamaños de textos.
     */
    public static Font Portada = new Font(Font.FontFamily.COURIER, 14, Font.BOLD);
    public static Font Titulos = new Font(Font.FontFamily.COURIER, 14, Font.BOLD);
    public static Font SubTitulos = new Font(Font.FontFamily.COURIER, 12, Font.BOLD);
    public static Font Textos = new Font(Font.FontFamily.COURIER, 11, Font.NORMAL);
    public static Font Elementos = new Font(Font.FontFamily.COURIER, 10, Font.ITALIC);
    public static Font TblColumnas = new Font(Font.FontFamily.COURIER, 11, Font.BOLD);
}
