package io.parrotsoftware.parrotchallengeservice.util;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Element;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;

/**
 * @description Clase que contiene la definición de métodos para retorno de textos con diferentes formatos.
 * @created 13-02-2021
 * @author Luis Zepeda (zepedagonzalezluis@gmail.com)
 */
public class Titles extends Fonts {

    /**
     * Método que permite configurar títulos.
     * @param title Nombre o texto titulo.
     * @return Titulo configurado.
     */
    public static Paragraph textTitle(final String title){
        Paragraph parrafo = new Paragraph();
        Chunk parte = new Chunk();
        parrafo.setAlignment(Element.ALIGN_CENTER);
        parte.append(
                title.toUpperCase() +
                        "\n\n"
        );
        parte.setFont(Titulos);
        parrafo.add(parte);
        return parrafo;
    }

    /**
     * Método que permite configurar subtítulos.
     * @param subtitulo Nombre o texto subtitulo.
     * @return Subtitulo configurado.
     */
    public static Paragraph textSubTitle(final String subtitulo){
        Paragraph parrafo = new Paragraph();
        Chunk parte = new Chunk();
        parrafo.setAlignment(Element.ALIGN_LEFT);
        parte.append(
                subtitulo +
                        "\n\n"
        );
        parte.setFont(SubTitulos);
        parrafo.add(parte);
        return parrafo;
    }

    /**
     * Método que permite configurar los textos de un párrafo.
     * @param texto Texto de un párrafo.
     * @return Párrafo configurado.
     */
    public static Phrase texts(final String texto){
        Phrase parrafo = new Paragraph();
        Chunk parte = new Chunk();
        //parrafo.setAlignment(Element.ALIGN_JUSTIFIED);
        parte.append(
                texto +
                        "\n\n"
        );
        parte.setFont(Textos);
        parrafo.add(parte);
        return parrafo;
    }

    /**
     * Método que permite configurar los títulos de elementos como tablas,
     * imagenes, etc.
     * @param tituloelemento Nombre del elemento.
     * @return Nombre configurado.
     */
    public static Paragraph titlesElements(final String tituloelemento){
        Paragraph parrafo = new Paragraph();
        Chunk parte = new Chunk();
        parrafo.setAlignment(Element.ALIGN_CENTER);
        parte.append(
                tituloelemento +
                        "\n\n"
        );
        parte.setFont(Elementos);
        parrafo.add(parte);
        return parrafo;
    }
}
