package io.parrotsoftware.parrotchallengeservice.util;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import io.parrotsoftware.parrotchallengeservice.model.report.ProductReport;
import io.parrotsoftware.parrotchallengeservice.model.report.Report;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

import static io.parrotsoftware.parrotchallengeservice.util.Titles.texts;

@Slf4j
public class Tables extends Fonts {

    public static PdfPTable reportTableVentas (List<Report> reportList){

        String [] columnas = {
                "Nombre producto",
                "Cantidad total",
                "Precio total"
        };
        PdfPTable tabla = new PdfPTable(columnas.length);

        PdfPCell columna;

        for (String column : columnas) {
            columna = (new PdfPCell(tableTitle(column)));
            columna.setBackgroundColor(BaseColor.LIGHT_GRAY);
            tabla.addCell(columna);
        }

        for (Report report : reportList) {
            for (ProductReport productReport : report.getProductReportList()) {
                columna = (new PdfPCell(texts(productReport.getProductName()))); tabla.addCell(columna);
                columna = (new PdfPCell(texts(String.valueOf(productReport.getQuantity())))); tabla.addCell(columna);
                columna = (new PdfPCell(texts(String.valueOf(productReport.getTotalAmount())))); tabla.addCell(columna);
            }
        }

        float [] columnWidth = {2f, 2f, 2f};

        try {

            tabla.setWidths (columnWidth);

        } catch (DocumentException ex) {
            log.error("Error al agregar registros en la tabla del reporte. {}", ex.toString());
        }

        return tabla;
    }

    public static Paragraph tableTitle(final String texto){
        Paragraph parrafo = new Paragraph();
        Chunk parte = new Chunk();
        parrafo.setAlignment(Element.ALIGN_CENTER);
        parte.append(
                "\n" +
                        texto +
                        "\n\n"
        );
        parte.setFont(TblColumnas);
        parrafo.add(parte);
        return parrafo;
    }

}
