package io.parrotsoftware.parrotchallengeservice.util;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import io.parrotsoftware.parrotchallengeservice.model.report.ReportResponse;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import static io.parrotsoftware.parrotchallengeservice.util.Tables.reportTableVentas;
import static io.parrotsoftware.parrotchallengeservice.util.Titles.textSubTitle;
import static io.parrotsoftware.parrotchallengeservice.util.Titles.textTitle;

@Slf4j
public class ReportPDF {

    public static void createPdf(final String fileName, final ReportResponse reportResponse) {

        try {

            File file = new File(fileName);

            /*--- Configuración del archivo ---*/
            Document pdfDocument = new Document(PageSize.A4, 40, 40, 40, 40);
            PdfWriter writer = PdfWriter.getInstance(pdfDocument, new FileOutputStream(file));
            pdfDocument.open();
            writer.setStrictImageSequence(true);

            /*--- Información definición del problema ---*/
            pdfDocument.add(textTitle("Reporte"));
            pdfDocument.add(textSubTitle("I. Productos vendidos: ".toUpperCase()));
            pdfDocument.add(reportTableVentas(reportResponse.getReportList()));

            pdfDocument.close();

        } catch (DocumentException | IOException ex) {
            log.error("Error al crear el reporte");
        }
    }
}
