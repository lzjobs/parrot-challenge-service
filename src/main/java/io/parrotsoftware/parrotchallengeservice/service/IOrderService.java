package io.parrotsoftware.parrotchallengeservice.service;

import io.parrotsoftware.parrotchallengeservice.model.OrdenResponse;
import io.parrotsoftware.parrotchallengeservice.model.OrderRequest;
import reactor.core.publisher.Mono;

import java.util.Map;

public interface IOrderService {
    Mono<OrdenResponse> createOrder (final Map<String, String> headers, final OrderRequest orderRequest);
}
