package io.parrotsoftware.parrotchallengeservice.service;

import com.pinncode.libs.exceptionhandler.exception.CodeException;
import io.parrotsoftware.parrotchallengeservice.model.OrderProduct;
import io.parrotsoftware.parrotchallengeservice.model.Orders;
import io.parrotsoftware.parrotchallengeservice.model.Users;
import io.parrotsoftware.parrotchallengeservice.model.report.ProductReport;
import io.parrotsoftware.parrotchallengeservice.model.report.Report;
import io.parrotsoftware.parrotchallengeservice.model.report.ReportResponse;
import io.parrotsoftware.parrotchallengeservice.repository.IOrderProductRepository;
import io.parrotsoftware.parrotchallengeservice.repository.IOrderRepository;
import io.parrotsoftware.parrotchallengeservice.repository.IUserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static io.parrotsoftware.parrotchallengeservice.util.ReportPDF.createPdf;
import static io.parrotsoftware.parrotchallengeservice.validation.ParrotValidations.validateAuthorization;
import static io.parrotsoftware.parrotchallengeservice.validation.ParrotValidations.validateQueryDates;

@Slf4j
@Service
public class ReportService implements IReportService {

    @Value("${path-report:}")
    private String pathReport;

    @Value("${fileName:}")
    private String fileName;

    @Autowired
    private IOrderRepository orderRepository;

    @Autowired
    private IUserRepository userRepository;

    @Autowired
    private IOrderProductRepository orderProductRepository;

    @Override
    public Mono<ReportResponse> getDataForReport(Map<String, String> headers, String startDate, String endDate) {
        return validateAuthorization(headers)
                .flatMap(aBoolean -> validateQueryDates(startDate, endDate)
                .flatMap(aBoolean1 -> Mono.just(orderRepository.getOrdersBySaleDate(startDate, endDate))
                .flatMap(orders ->  {

                    log.info("Orders :: {}", orders);

                    return getProductsByOrders(orders);
                })));
    }

    private Mono<ReportResponse> getProductsByOrders (final List<Orders> ordersList) {

        if (CollectionUtils.isEmpty(ordersList)) {
            return Mono.error(new CodeException("", "", HttpStatus.NO_CONTENT));
        }

        List<Report> reportList = new ArrayList<>();

        ordersList.forEach(order -> {

            Integer idOrder = order.getIdOrder();
            final String dateOrder = order.getCreatedTime();
            Integer idUser = order.getIdUser();

            Users user = userRepository.getUserById(idUser);
            List<OrderProduct> productList = orderProductRepository.getOrderProductsByIdOrder(idOrder);

            reportList.add(getReport(user, dateOrder, productList));
        });

        final String file = pathReport.concat("/").concat(fileName);

        ReportResponse reportResponse = new ReportResponse();
        reportResponse.setReportList(reportList);
        reportResponse.setReportFile(file);

        createPdf(file, reportResponse);

        return Mono.just(reportResponse);
    }

    private Report getReport (Users user, final String dateOrder, List<OrderProduct> productList) {

        List<ProductReport> productReportList = new ArrayList<>();
        List<Float> totales = new ArrayList<>();

        productList.forEach(product -> {

            Float totalAmount = product.getQuantity() * product.getUnitPrice();

            ProductReport productReport = new ProductReport();
            productReport.setProductName(product.getProductName());
            productReport.setUnitPrice(product.getUnitPrice());
            productReport.setQuantity(product.getQuantity());
            productReport.setTotalAmount(totalAmount);

            productReportList.add(productReport);
            totales.add(totalAmount);

        });

        Report report = new Report();

        report.setDate(dateOrder);
        report.setUserName(user.getName().concat(" ").concat(user.getLastName()));
        report.setTotalAmount((float) totales.stream().mapToDouble(o -> o).sum());
        report.setProductReportList(orderListByTotalAmount(productReportList));

        return report;
    }

    private List<ProductReport> orderListByTotalAmount (List<ProductReport> productReportList) {

        Comparator<ProductReport> compareByTotalAmount = Comparator.comparing(ProductReport::getTotalAmount).reversed();

        return productReportList.stream().sorted(compareByTotalAmount).collect(Collectors.toList());
    }
}
