package io.parrotsoftware.parrotchallengeservice.service;

import io.parrotsoftware.parrotchallengeservice.model.report.Report;
import io.parrotsoftware.parrotchallengeservice.model.report.ReportResponse;
import reactor.core.publisher.Mono;

import java.util.Map;

public interface IReportService {
    Mono<ReportResponse> getDataForReport (final Map<String, String> headers, final String startDate, final String endDate);
}
