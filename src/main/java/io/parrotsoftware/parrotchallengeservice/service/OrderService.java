package io.parrotsoftware.parrotchallengeservice.service;

import com.pinncode.libs.exceptionhandler.exception.CodeException;
import io.parrotsoftware.parrotchallengeservice.model.OrdenResponse;
import io.parrotsoftware.parrotchallengeservice.model.OrderRequest;
import io.parrotsoftware.parrotchallengeservice.model.Products;
import io.parrotsoftware.parrotchallengeservice.model.Users;
import io.parrotsoftware.parrotchallengeservice.model.order.Order;
import io.parrotsoftware.parrotchallengeservice.repository.*;
import io.parrotsoftware.parrotchallengeservice.transform.OrderTransform;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static io.parrotsoftware.parrotchallengeservice.exception.ParrotErrors.PARROT_ERR_003;
import static io.parrotsoftware.parrotchallengeservice.validation.ParrotValidations.validateOrder;

@Slf4j
@Service
public class OrderService implements IOrderService {

    @Autowired
    private IComensalRepository comensalRepository;

    @Autowired
    private ICommonRepository commonRepository;

    @Autowired
    private IOrderRepository orderRepository;

    @Autowired
    private IProductsRepository productsRepository;

    @Autowired
    private IOrderProductRepository orderProductRepository;

    @Autowired
    private OrderTransform orderTransform;

    @Override
    public Mono<OrdenResponse> createOrder(final Map<String, String> headers, final OrderRequest orderRequest) {

        final Integer idUser = Optional.of(orderRequest).map(OrderRequest::getOrden).map(Order::getUser).map(Users::getIdUser).orElse(1);

        return validateOrder(headers, orderRequest)
                .flatMap(aBoolean -> Mono.just(comensalRepository.createComensal(orderRequest.getOrden().getComensal()))
                .flatMap(bBoolean -> Mono.just(commonRepository.getLastIdentifier("comensal", "idComensal"))
                .flatMap(idComensal -> Mono.just(orderRepository.createOrder(idUser, idComensal))
                .flatMap(dBoolean -> Mono.just(commonRepository.getLastIdentifier("orders", "idOrder"))
                .flatMap(idOrder ->  registerProductsAndOrderProducts(idOrder, orderRequest.getOrden().getProductos())
                .flatMap(eBoolean -> {

                    if (Boolean.TRUE.equals(eBoolean)) {
                        return Mono.error(new CodeException(PARROT_ERR_003.getCode(), PARROT_ERR_003.getMessage(), HttpStatus.UNAUTHORIZED));
                    }

                    return Mono.just(orderTransform.responseOrder(orderRequest, idOrder));

                }))))));
    }

    private Mono<Boolean> registerProductsAndOrderProducts (final Integer idOrder, final List<Products> products) {

        List<Products> productos = Optional.ofNullable(products).orElse(null);
        List<Boolean> booleans = new ArrayList<>();

        if (!CollectionUtils.isEmpty(productos)) {
            productos.forEach(product -> {

                Boolean productCreated = productsRepository.createProduct(product);
                Integer productId = commonRepository.getLastIdentifier("products", "idProducto");
                Boolean orderProductCreated = orderProductRepository.createOrderProduct(idOrder, productId, product);

                booleans.add(productCreated && orderProductCreated);
            });
        }

        return Mono.just(containsFalse(booleans));
    }

    public boolean containsFalse(List<Boolean> booleans){
        return booleans.stream().anyMatch(o -> o.equals(false));
    }
}
